import Head from "next/head";
import Navbar from './Navbar';


const Layout = () => (
    <>
       <Head>
           <title> Bitcoin Price </title>
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/cerulean/bootstrap.min.css" />
        </Head>
        <Navbar />
    </>
)

export default Layout;