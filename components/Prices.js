class Prices extends React.Component {

    constructor(props){
        super(props);
    }

    state = {
        currency: 'EUR'
    }

    render() {
        return (
            <div>
                <ul className="list-group">
                    <li className="list-group-item">
                        Bit coin rate for {this.props.priceData ? this.props.priceData.bpi[this.state.currency].description : null} : 
                        <strong> 
                            {
                                this.props.priceData ? this.props.priceData.bpi[this.state.currency].rate : null
                            }
                        </strong>
                    </li>
                </ul>
                <select onChange={e => this.setState({currency: e.target.value})} className="form-control">
                    <option value="USD"> USD </option>
                    <option value="GBP"> GBP </option>
                    <option value="EUR"> EUR </option>
                </select>
            </div>
        )
    }

}

export default Prices;