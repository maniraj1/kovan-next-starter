import Head from 'next/head'
import styles from '../styles/Home.module.css';
import React, { useState, useEffect } from "react";
import Layout from '../components/Layout';
import Prices from "../components/Prices";

export default function Home() {

  const [priceData, setPriceData] = useState();


  React.useEffect(() => {
    const res = fetch("https://api.coindesk.com/v1/bpi/currentprice.json");
    res.then(result => result.json())
    .then((data) => {
      setPriceData(data)
    })

  }, []);
  return (
    <div>
      <h1> Welcome to Bitz Price kovan</h1>
      <Layout />
      <Prices priceData={priceData ? priceData : null}/>
    </div>
  )
}
